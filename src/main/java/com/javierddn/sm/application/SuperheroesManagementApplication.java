package com.javierddn.sm.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuperheroesManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(SuperheroesManagementApplication.class, args);
    }

}
